# You will be able to

1. **Structure an HTML page** using doctype, html, head, title, and body tags
2. Add an **image**, **link**, **paragraph** text, and **header** text to a page
3. Create an **ordered** and **unordered list** using the **ol**, **ul**, **li** tags
4. **Style** an HTML page using CSS
5. **Organize** sections of HTML with **div** tags

# HTML

<img src="http://curriculum-content.s3.amazonaws.com/KWK/html-intro-baby.png" alt="baby reading book HTML for Babies" align="right" hspace="10" height="200" />

HTML stands for **Hypertext Markup Language**, and every single website on the internet is written in HTML. Every. Single. Page. HTML *provides the skeleton for websites* and organizes the different elements on a page into categories like image, paragraph, and title.

HTML has a specific syntax that's a bit different from Ruby, but you'll get a hang of it quickly. It's not too hard, at all. Most commonly, you'll write HTML in pairs of bracketed tags like this:

```

<title> My Awesome Webpage </title>

```
